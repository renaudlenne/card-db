defmodule CardsDb.Repo.Migrations.CreateCardTypes do
  use Ecto.Migration

  def change do
    create table(:card_types) do
      add :name, :string

      timestamps()
    end
    alter table(:cards) do
      add :card_type_id, references(:card_types)
    end
  end
end
