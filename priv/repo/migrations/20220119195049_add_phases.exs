defmodule CardsDb.Repo.Migrations.AddPhases do
  use Ecto.Migration

  def change do
    alter table(:cards) do
      add :phase_id, references(:phases)
    end
  end
end
