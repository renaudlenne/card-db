defmodule CardsDb.Repo.Migrations.CreateCards do
  use Ecto.Migration

  def change do
    create table(:cards) do
      add :name, :string
      add :image, :string
      add :content, :text
      add :lore, :text

      timestamps()
    end
  end
end
