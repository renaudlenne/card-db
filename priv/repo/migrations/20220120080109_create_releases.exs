defmodule CardsDb.Repo.Migrations.CreateReleases do
  use Ecto.Migration

  def change do
    create table(:releases) do
      add :name, :string
      add :date, :naive_datetime

      timestamps()
    end
    alter table(:cards) do
      add :release_id, references(:releases)
    end
  end
end
