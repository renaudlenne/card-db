defmodule CardsDb.Main.Phase do
  use Ecto.Schema
  import Ecto.Changeset

  schema "phases" do
    field :name, :string

    has_many :cards, CardsDb.Main.Card

    timestamps()
  end

  @doc false
  def changeset(phase, attrs) do
    phase
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
