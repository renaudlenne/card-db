defmodule CardsDb.Main.CardAdmin do
  alias CardsDb.Main
  alias CardsDb.Repo

  def search_fields(_schema) do
    [
      :content,
      :lore,
      :name
    ]
  end

  def index(_) do
    [
      name: %{},
      image: %{},
      card_type_id: %{
        name: "Type",
        value: fn c -> Main.get_card_type!(c.card_type_id).name end,
        filters: Enum.map(Main.list_card_types(), fn t -> {t.name, t.id} end)
      },
      phase_id: %{
        name: "Phase",
        value: fn c -> Main.get_phase!(c.phase_id).name end,
        filters: Enum.map(Main.list_phases(), fn p -> {p.name, p.id} end)
      },
      release_id: %{
        name: "Release",
        value: fn c -> Main.get_release!(c.release_id).name end,
        filters: Enum.map(Main.list_releases(), fn r -> {r.name, r.id} end)
      },
    ]
  end

  def form_fields(_) do
    [
      name: %{},
      image: %{},
      card_type_id: %{},
      phase_id: %{},
      content: %{},
      release_id: %{},
      lore: %{type: :richtext},
    ]
  end

end