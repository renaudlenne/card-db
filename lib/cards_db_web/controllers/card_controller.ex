defmodule CardsDbWeb.CardController do
  use CardsDbWeb, :controller

  alias CardsDb.Main
  alias CardsDb.Main.Card

  def index(conn, _params) do
    cards = Main.list_cards()
    render(conn, "index.html", cards: cards)
  end

  def show(conn, %{"id" => id}) do
    card = Main.get_card!(id)
    render(conn, "show.html", card: card)
  end

end
