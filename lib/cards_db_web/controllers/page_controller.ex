defmodule CardsDbWeb.PageController do
  use CardsDbWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
